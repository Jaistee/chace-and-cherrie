<!DOCTYPE html>
<html lang="en">
<head>
<title>Cakes</title>
	  <meta charset="utf-8">
 	  <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
	<!-- Social Media Buttons -->
	  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<!-- Social Media Buttons -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="CSS/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="style.css">

<body data-spy="scroll" data-target=".navbar" data-offset="50">

<nav class="navbar navbar-expand-sm navbar-light sticky-top bg-pastelPink " id="my-navbar">
	    <a class="navbar-brand nav-item" href="Index.php"><img class="OwnLogo"src="images/FinalLogo.png" onclick=" ShowNavLink()" alt="Logo" style="width: 100px;"></a>
	    <button class="navbar-toggler " type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
	      <span class="navbar-toggler-icon"></span>
	    </button>

	    <div class="collapse navbar-collapse" id="navbarSupportedContent">
	      <ul class="navbar-nav mr-auto navdrop"  id="navlinks">

	         	<?php if(!isset($_SESSION['username'])){ ?>
	         	     <li class="nav-item"><a class="nav-link nav-menu" href="Index.php">Home </a></li>
	         <ul>

	         	     <li class="nav-item"><a class="nav-link " href="#"> Cakes </a>
	         	     	<ul>
	         	     	   <li class="nav-item"><a class="nav-link " href="Dessert.php"> Dessert </a>	
	         	     	   <li class="nav-item"><a class="nav-link " href="Beverages.php"> Beverages </a>	
	         	     	   <li class="nav-item"><a class="nav-link " href="CupCakes.php"> Pastries </a>	
	         	     	</ul>
	         	     </li>
	         	     


	         	 <?php } ?> 
	         </ul>

	      </ul>
	       
	     <form class="form-inline" action="#">
	      
	       <?php if (isset($_SESSION['username'])){ ?>
	          <input type="email" class="form-control" id="email" placeholder="Email">
	          <input type="password" class="form-control" id="pwd" placeholder="Password">
	          <button type="submit" class="btn btn-pastelPink">Submit</button>
	        <?php } ?> 
	     
	     </form>
	    </div>
</nav>

	<!-- Image Grid Begins -->
		<section class="section-padding" id="ImageGrid" >
			<div class="container">
				 <div class="row">
				 	<div class="col-sm-12">
				 		<br><br>
				 		<h2 class="title text-center"> 
				 			Our own <span class="decorate">Dessert</span> Special for you
				 		</h2>
				 	</div>
				 </div>

				 <div class="container">
				 	<div id="my-grid" class="grid-padding">
				 		<div class="row">
				 			<!-- First Colum -->
				 			<div class="col-sm-4">

				 				<div class="row showcase-left"><img src="Images/Dessert/1.jpg" class="fit-image" onclick="openModal();currentSlide(1)">
				 				</div>
				 				<div class="row showcase-right"><img src="Images/Dessert/2.jpg" class="fit-image" onclick="openModal();currentSlide(2)">
				 				</div>
				 				<div class="row showcase-top"><img src="Images/Dessert/3.jpg" class="fit-image" onclick="openModal();currentSlide(3)">
				 				</div>
				 				<div class="row showcase-bottom"><img src="Images/Dessert/4.jpg" class="fit-image" onclick="openModal();currentSlide(4)">
				 				</div>

				 			</div>
				 			<!-- Second Column -->
				 			<div class="col-sm-4">

				 				<div class="row showcase-top"><img src="Images/Dessert/5.jpg" class="fit-image" onclick="openModal();currentSlide(5)">
				 				</div>
				 				<div class="row showcase-bottom"><img src="Images/Dessert/6.jpg" class="fit-image" onclick="openModal();currentSlide(6)">
				 				</div>
				 				<div class="row showcase-left"><img src="Images/Dessert/7.jpg" class="fit-image" onclick="openModal();currentSlide(7)">
				 				</div>
				 				<div class="row showcase-right"><img src="Images/Dessert/8.jpg" class="fit-image" onclick="openModal();currentSlide(8)">
				 				</div>
				 				
				 			</div>
				 			<!-- Third Column -->
				 			<div class="col-sm-4">

				 				<div class="row showcase-right"><img src="Images/Dessert/9.jpg" class="fit-image" onclick="openModal();currentSlide(9)">
				 				</div>
				 				<div class="row showcase-bottom"><img src="Images/Dessert/10.jpg" class="fit-image" onclick="openModal();currentSlide(10)">
				 				</div>
				 				<div class="row showcase-top"><img src="Images/Dessert/11.jpg" class="fit-image" onclick="openModal();currentSlide(11)">
				 				</div>
				 				<div class="row showcase-left"><img src="Images/Dessert/12.jpg" class="fit-image" onclick="openModal();currentSlide(12">
				 				</div>
				 				
				 			</div>
				 		</div>
				 	</div>
				 </div>
			</div>
		</section>
	<!-- Image Grid Ends -->

	<!-- Modal for Full Screen Image -->
		<div id="my-modal" class="modal">
			<!-- close button -->
			<span class="exit" onclick="exitModal()">&times;</span>
			<!-- Modal -->
			<div class="modal-content">
				<!-- Image Viewer -->
				<div class="my-slides">
					<div class="numbertext">1/12</div>
					<img src="Images/Dessert/1.jpg" class="portrait-image center">
				</div>
				<div class="my-slides">
					<div class="numbertext">2/12</div>
					<img src="Images/Dessert/2.jpg" class="landscape-image">
				</div>
				<div class="my-slides">
					<div class="numbertext">3/12</div>
					<img src="Images/Dessert/3.jpg" class="portrait-image center">
				</div>
				<div class="my-slides">
					<div class="numbertext">4/12</div>
					<img src="Images/Dessert/4.jpg" class="portrait-image center">
				</div>
				<div class="my-slides">
					<div class="numbertext">5/12</div>
					<img src="Images/Dessert/5.jpg" class="landscape-image">
				</div>
				<div class="my-slides">
					<div class="numbertext">6/12</div>
					<img src="Images/Dessert/6.jpg" class="portrait-image center" >
				</div>
				<div class="my-slides">
					<div class="numbertext">7/12</div>
					<img src="Images/Dessert/7.jpg" class="landscape-image">
				</div>
				<div class="my-slides">
					<div class="numbertext">8/12</div>
					<img src="Images/Dessert/8.jpg" class="portrait-image center">
				</div>
				<div class="my-slides">
					<div class="numbertext">9/12</div>
					<img src="Images/Dessert/9.jpg" class="landscape-image">
				</div>
				<div class="my-slides">
					<div class="numbertext">10/12</div>
					<img src="Images/Dessert/10.jpg" class="landscape-image">
				</div>
				<div class="my-slides">
					<div class="numbertext">11/12</div>
					<img src="Images/Dessert/11.jpg" class="landscape-image">
				</div>
				<div class="my-slides">
					<div class="numbertext">12/12</div>
					<img src="Images/Dessert/12.jpg" class="portrait-image center">
				</div>
				<!-- Next and Prev -->
				<a class="prevbuttons" onclick="changeSlides(-1)">&#10094;</a>
				<a class="nextbutton" onclick="changeSlides(1)">&#10095;</a>
			
			</div>
		</div>
	<!-- Modal for Full Screen Image -->

	<header><?php include ( "./include/Footer.php" ); ?></header>
	<button id="back-to-top-btn"><p>&#11161;</p></button>
	
</body>

</html>

<style>
	/*Cake.php*/
		/*Cake Grid */
		#ImageGrid{
			background-color: #F5F5F5
			}
			.fit-image{
			height: 40%;
			width: 100%;
			top: 0;
			background-attachment: fixed;
			background-position: center;
			background-repeat: no-repeat;
			background-size: cover;
			}
			.title{
			font-size: 24px;
			margin-bottom: 50px;
			}
			#my-grid{
			margin-top: 50px;
			}
			.grid-padding{
			padding: 0 8px;
			}
			.grid-padding img{
			margin-bottom: 15px;
			transition: 0.8s ease;
			}
			.grid-padding img:hover{
			box-shadow: 0 10px 40px 0 rgba(0,0,0,0.5);
			transition: 0.8s ease;
			cursor: pointer;
			}
			.col-sm-4{
			padding-right: 30px;
			}

		/*Cake Grid */
		
		/*Modal*/
			.modal{
			display: none;
			position: fixed;
			z-index: 1;
			padding: 5px 0;
			left: 0;
			top: 0;
			width: 100%;
			height: 100%;
			overflow: auto;
			background-color: rgba(0,0,0,0.8);
			}
			.modal-content{
			position: relative;
			margin: auto;
			padding: 0;
			width: 85%;
			max-width: 1450px;
			border: none;
			background-color: transparent;
			}
			.my-Slides{
			text-align: center;
			}

      .center {
       display: block;
       margin-left: auto;
       margin-right: auto;
      }

			/*Image Orientation*/
			.portrait-image{
			height: 98vh;

			}
			.landscape-image{
			width: 76vw;
			}

			/*Exit button*/
			.exit{
			color: #c90018 !important ;
			cursor: pointer;
			position: absolute;
			top: 150px;
			right: 100px;
			width: auto;
			padding: 16px;
			margin-top: -50px;
			font-weight: bold;
			font-size: 3em;
			user-select: none;
			-webkit-user-select:none;
			}
			.exit:hover, .exit:focus{
			color: #FFF;
			transition: 0.6s ease;
			font-size: 4em;
			text-decoration: none !important;
			cursor: pointer !important;
			}

			/*Next and Prev*/
			.prevbuttons, .nextbutton {
			cursor: pointer;
			position: absolute;
			top: 50%;
			width: auto;
			padding: 16px;
			margin-top: -50px;
			color: #FFF !important;
			font-weight: bold;
			font-size: 3em;
			transition: 0.6s ease;
			border-radius: 0 3px 3px 0;
			user-select: none;
			-webkit-user-select:none;
			}
			/*Postion the  next button to the right*/
			.nextbutton{
			left: 1400px;
			border-radius: 3px 0 0 3px;
			}

			.nextbutton:hover, .prevbuttons:hover{
			background-color: rgba(0,0,0,0.8);
			}

			/*number text*/
			.numbertext{
			color: grey;
			font-size: 18px;
			font-family: Lato, sans-serif;
			padding: 8px  50px;
			position: absolute;
			top:0;
			left: 10px;
			}
		/*Modal*/
	/*Cake.php*/
</style>


<!-- license script -->
<script src="JS/scrollreveal.js"></script>
<!-- license script -->


<!-- Own Script -->
<script type="text/javascript" src="JS/ScrollAnimation.js"></script>
<script type="text/javascript" src="JS/mainscript.js"></script>
<script type="text/javascript" src="JS/backtotop.js"></script>
<!-- Own Script -->
<script type="text/javascript" src="JS/jquery.smoothscroll.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" src="JS/bootstrap.bundle.js"></script>
<script type="text/javascript" src="JS/bootstrap.bundle.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<!-- Cake.php -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" src="JS/jquery.smoothscroll.js"></script>
<!-- Cake.php -->

<script>
	
 window.addEventListener('scroll', () => {
    var top = 0;
    var a = 162.5;
    const scrollPosition = document.documentElement.scrollHeight - window.innerHeight;
    const scrolled = window.scrollY;

    console.log(scrolled);
    if ( top === Math.ceil(scrolled)) {
      document.getElementById('my-navbar').style.display = "flex";
      
    }else if ( a < Math.ceil(scrolled) || a > Math.ceil(scrolled)){
       document.getElementById('my-navbar').style.opacity = 1;
    }else if (Math.ceil(scrolled) === a  ){
    	document.getElementById('my-navbar').style.opacity = 0.5;
    }
   })

</script>

<script type="text/javascript">

	// $('html, body').smoothScroll(1000);

	// Opening Modal script
	function openModal(){
		// Open on click
		document.getElementById('my-modal').style.display = "inline-flex";
		document.body.style.overflow = "hidden";
		document.getElementById('my-navbar').style.display = "none";
	}
	// Close modal script
	function exitModal(){
		document.getElementById('my-modal').style.display = "none";
		document.body.style.overflow = "auto";
		document.getElementById('my-navbar').style.display = "flex";
	}

	// variable for slide
	var slideIndexJS = 1;

	// Calling function
	showSlides( slideIndexJS );

	// Next and prevt script
	function changeSlides(n){
		showSlides(slideIndexJS += n);
	}
	// Display Image that was clicked
	function currentSlide(n){
		showSlides(slideIndexJS = n);
	}

	function showSlides(n){
		var i;

		// get all class at my-slides
		var slidesJS = document.getElementsByClassName("my-slides");

		// OverFlow
		if (n > slidesJS.length) { slideIndexJS = 1; }
		// UnderFlow
		if (n < 1) {slideIndexJS = slidesJS.length;  }

		// hide image
		for (i = 0; i < slidesJS.length; i++){
			slidesJS[i].style.display  = "none";
		}

		// showing slide which click
		slidesJS[slideIndexJS - 1].style.display = "block";
	}
</script>