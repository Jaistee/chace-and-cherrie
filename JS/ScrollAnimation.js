
window.sr = ScrollReveal();
	sr.reveal('.navbar', {
		duration:2000,
		origin:'bottom'
	});

	sr.reveal('.OwnLogo', {
		duration:2000,
		delay:300,
		origin:'top'
	});
	sr.reveal('.scrolldownTop', {
		duration:2000,
		delay:2000,
		origin:'top'
	});

	sr.reveal('.showcase-left',{
		duration: 2000,
		delay:800,
		origin: 'left',
		distance: '300px',
		viewFactor: 0.2
	});

	sr.reveal('.showcase-right',{
		duration: 2000,
		delay:800,
		origin: 'right',
		distance: '300px',
		viewFactor: 0.2
	});

	sr.reveal('.showcase-top',{
		duration: 2000,
		origin: 'top',
		distance: '300px',
		viewFactor: 0.2
	});

	sr.reveal('.showcase-bottom',{
		duration: 2000,
		delay:600,
		origin: 'bottom',
		distance: '300px',
		viewFactor: 0.2
	});

	sr.reveal('.showcase-fade',{
		duration: 2000,
		origin: 'top',
		distance: '300px',
		viewFactor: 0.2
	});

	sr.reveal('.CakeOne-left',{
		duration: 2000,
		delay:400,
		origin: 'left',
		distance: '100px',
		viewFactor: 0.3
	});

	sr.reveal('.CakeTwo-left',{
		duration: 2000,
		delay:600,
		origin: 'left',
		distance: '300px',
		viewFactor: 0.3
	});

	sr.reveal('.CakeThree-left',{
		duration: 2000,
		delay:700,
		origin: 'left',
		distance: '500px',
		viewFactor: 0.3
	});

	sr.reveal('.CakeFour-left',{
		duration: 2000,
		delay:800,
		origin: 'left',
		distance: '700px',
		viewFactor: 0.3
	});

	sr.reveal('.CakeFive-left',{
		duration: 2000,
		delay:900,
		origin: 'left',
		distance: '900px',
		viewFactor: 0.3
	});

	sr.reveal('.CakeSix-left',{
		duration: 2000,
		delay:1000,
		origin: 'left',
		distance: '1100px',
		viewFactor: 0.3
	});

	