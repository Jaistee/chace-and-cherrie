<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Document</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
 <!-- Social Media Buttons -->
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
 <!-- Social Media Buttons -->
 <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
 <link rel="stylesheet" href="./SwiperCSS/swiper.min.css">
 <link rel="stylesheet" type="text/css" href="CSS/bootstrap.min.css">
 <link rel="stylesheet" type="text/css" href="style.css">

<body data-spy="scroll" data-target=".navbar" data-offset="50">



  <section class="banner ">
		 <div class="banner-social-icons" style="margin-left: 80%;">          
			  <ul>
				<li><a href="#"><i class="fa fa-facebook"></i></a></li>
				<li><a href="#"><i class="fa fa-twitter"></i></a></li>
				<li><a href="#"><i class="fa fa-instagram"></i></a></li>
				<li><a href="#"><i class="fa fa-youtube"></i></a></li>
			  </ul>
		  </div>    
		  <div class="greetings showcase-bottom">
		  <p><span class="greetingsbox">Welcome</span></p>
		  <span class="greetingsboxs scrolldownTop">Scroll Down</span>
		  </div>
   </section>



<nav class="navbar navbar-expand-sm navbar-light sticky-top bg-pastelPink " id="my-navbar">
	 <a class="navbar-brand nav-item" href="Index.php"><img class="OwnLogo"src="images/FinalLogo.png" onclick=" ShowNavLink()" alt="Logo" style="width: 100px;"></a>
	 <button class="navbar-toggler " type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
	   <span class="navbar-toggler-icon"></span>
	 </button>

	 <div class="collapse navbar-collapse" id="navbarSupportedContent">
	   <ul class="navbar-nav mr-auto navdrop"  id="navlinks">

			  <?php if(!isset($_SESSION['username'])){ ?>
				   <li class="nav-item"><a class="nav-link nav-menu" href="Index.php">Home </a></li>
		  <ul>

				   <li class="nav-item"><a class="nav-link " href="#"> Products </a>
					   <ul>
						  <li class="nav-item"><a class="nav-link " href="Dessert.php"> Dessert </a>	
						  <li class="nav-item"><a class="nav-link " href="Beverages.php"> Beverages </a>	
						  <li class="nav-item"><a class="nav-link " href="CupCakes.php"> Pastries </a>	
					   </ul>
				   </li>
				   


			   <?php } ?> 
		  </ul>

	   </ul>

	 </div>
</nav>


 <header><?php include ( "./include/Carousel.php"); ?></header>
 <header><?php include ( "./include/NewsUpdate.php"); ?></header>
 <header><?php include ( "./include/main.php"); ?></header>
 <header><?php include ( "./include/Footer.php" ); ?></header>
 <button id="back-to-top-btn"><p>&#11161;</p></button>
 
<body>
	
</body>
</html>

<!-- license script -->
<script src="JS/scrollreveal.js"></script>
<!-- license script -->


<!-- Own Script -->
<script type="text/javascript" src="JS/ScrollAnimation.js"></script>
<script type="text/javascript" src="JS/mainscript.js"></script>
<script type="text/javascript" src="JS/backtotop.js"></script>
<!-- Own Script -->
<script src="./SwiperJS/swiper.min.js"></script>
<script src="JS/jquery.smoothscroll.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="JS/bootstrap.bundle.js"></script>
<script src="JS/bootstrap.bundle.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<!-- Cake.php -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" src="JS/jquery.smoothscroll.js"></script>
<!-- Cake.php -->

<script>
	
 window.addEventListener('scroll', () => {
    var top = 0;
    var a = 162.5;
    const scrollPosition = document.documentElement.scrollHeight - window.innerHeight;
    const scrolled = window.scrollY;

    // console.log(scrolled);
    if ( top === Math.ceil(scrolled)) {
      document.getElementById('my-navbar').style.display = "flex";
      
    }else if ( a < Math.ceil(scrolled) || a > Math.ceil(scrolled)){
       document.getElementById('my-navbar').style.opacity = 1;
    }else if (Math.ceil(scrolled) === a  ){
    	document.getElementById('my-navbar').style.opacity = 0.5;
    }
   })

</script>

