<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Cup Cakes</title>
  <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
  <!-- Social Media Buttons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <!-- Social Media Buttons -->
  <link rel="stylesheet" href="./SwiperCSS/swiper.min.css">
  <link rel="stylesheet" type="text/css" href="CSS/bootstrap.min.css">
  <link rel="stylesheet" href="style.css">
   <!-- Dont transfer to style.css because of same define name with differ function -->
   <style>
    html, body {
      position: relative;
      height: 100%;
    }
    body {
      font-size: 14px;
      color:#000;
     
    }
    .fit-image{
	  height: 100%;
	  width: 100%;
      top: 0;
      
    }
    
    .caption {
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0, 0, 0, 0.4); /* Black w/opacity/see-through */
    border-radius: 10px;
    color: white;
    font-weight: bold;
    position: absolute;
    top: 80%;
    left: 50%;
    transform: translate(-50%, -50%);
    z-index: 2;
    width: 50%;
    padding: 40px;
    text-align: center;
    font-size:30px
    }
    
    .swiper-container {
      margin-top:5%;
      margin-bottom:5%;
      width: 40%;
      height: 75%;
    }
    .swiper-slide {
      background-position: center;
      background-size: cover;
    }
  </style>
</head>
<body>

    
<nav class="navbar navbar-expand-sm navbar-light sticky-top bg-pastelPink " id="my-navbar">
	 <a class="navbar-brand nav-item" href="Index.php"><img class="OwnLogo"src="images/FinalLogo.png" onclick=" ShowNavLink()" alt="Logo" style="width: 100px;"></a>
	 <button class="navbar-toggler " type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
	   <span class="navbar-toggler-icon"></span>
	 </button>

	 <div class="collapse navbar-collapse" id="navbarSupportedContent">
	   <ul class="navbar-nav mr-auto navdrop"  id="navlinks">

			  <?php if(!isset($_SESSION['username'])){ ?>
				   <li class="nav-item"><a class="nav-link nav-menu" href="Index.php">Home </a></li>
		  <ul>

				   <li class="nav-item"><a class="nav-link " href="#"> Products </a>
					   <ul>
						  <li class="nav-item"><a class="nav-link " href="Dessert.php"> Dessert </a>	
						  <li class="nav-item"><a class="nav-link " href="Beverages.php"> Beverages </a>	
						  <li class="nav-item"><a class="nav-link " href="CupCakes.php"> Pastries </a>	
					   </ul>
				   </li>
				   


			   <?php } ?> 
		  </ul>

	   </ul>

	 </div>
</nav>

<section id="introduction">

  <div class="CupcakeBG">
          
  </div>

  <div class="caption">
     <p>Chace and Cherrie Pastries</p>
  </div>

     
</section>



  <!-- Swiper -->
  <div class="swiper-container">
    <div class="swiper-wrapper">
    <div class="swiper-slide"> 
        <div class="caption">
             <p>CupCake</p>
        </div>
        <img class="fit-image" src="./Images/CupCakes/1.jpg">
    </div>
    <div class="swiper-slide">
        <div class="caption">
             <p>CupCake </p>
        </div>
         <img class="fit-image" src="./Images/CupCakes/2.jpg">
    </div>
    <div class="swiper-slide">
        <div class="caption">
             <p>CupCake </p>
        </div>
         <img class="fit-image" src="./Images/CupCakes/3.jpg">
    </div>
    <div class="swiper-slide">
        <div class="caption">
             <p>CupCake </p>
        </div>
         <img class="fit-image" src="./Images/CupCakes/4.jpg">
    </div>
    <div class="swiper-slide">
        <div class="caption">
             <p>CupCake </p>
        </div>
         <img class="fit-image" src="./Images/CupCakes/5.jpg">
    </div>
    <div class="swiper-slide">
         <div class="caption">
             <p>CupCake </p>
        </div>
         <img class="fit-image" src="./Images/CupCakes/6.jpg">
    </div>
    <div class="swiper-slide">
         <div class="caption">
             <p>CupCake </p>
        </div>
         <img class="fit-image" src="./Images/CupCakes/7.jpg">
    </div>
    <div class="swiper-slide">
         <div class="caption">
             <p>CupCake </p>
        </div>
         <img class="fit-image" src="./Images/CupCakes/8.jpg">
    </div>
    <div class="swiper-slide">
      <div class="caption">
             <p>CupCake </p>
        </div>
         <img class="fit-image" src="./Images/CupCakes/9.jpg">
    </div>
    <div class="swiper-slide"> 
        <div class="caption">
             <p>BundleCupCake</p>
        </div>
        <img class="fit-image" src="./Images/CupCakes/10.jpg">
    </div>
     <div class="swiper-slide"> 
        <div class="caption">
             <p>Blueberry Cake</p>
        </div>
        <img class="fit-image" src="./Images/CupCakes/11.jpg">
    </div>
    </div>

    <!-- Add Pagination -->
    <div class="swiper-pagination swiper-pagination-white"></div>
    <!-- Add Arrows -->
    <div class="swiper-button-next swiper-button-white"></div>
    <div class="swiper-button-prev swiper-button-white"></div>
  </div>

                
  <header><?php include ( "./include/Footer.php" ); ?></header>

</body>
</html>

<!-- license script -->
<script src="JS/scrollreveal.js"></script>
<!-- license script -->


<!-- Own Script -->
<script type="text/javascript" src="JS/ScrollAnimation.js"></script>
<script type="text/javascript" src="JS/mainscript.js"></script>
<script type="text/javascript" src="JS/backtotop.js"></script>
<!-- Own Script -->
<script type="text/javascript" src="JS/jquery.smoothscroll.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" src="JS/bootstrap.bundle.js"></script>
<script type="text/javascript" src="JS/bootstrap.bundle.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<!-- Cake.php -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" src="JS/jquery.smoothscroll.js"></script>
<!-- Cake.php -->

  <!-- Swiper JS -->
  <script src="./SwiperJS/swiper.min.js"></script>

<script>
  var swiper = new Swiper('.swiper-container', {
    spaceBetween: 30,
    effect: 'fade',
    pagination: {
      el: '.swiper-pagination',
      clickable: true,
    },
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
  });
</script>