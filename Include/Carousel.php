
<section id="Home" >
		<div id="carouselExampleInterval" class="carousel slide" data-ride="carousel">
		  <div class="carousel-inner">
		    <div class="carousel-item active">
				<div class="caption">
            		 <p>Owners</p>
     		   </div>
		      <img src="Images/Carousel/1.jpg" class="d-block w-100 h-100" alt="Cake1">
		    </div>
		    <div class="carousel-item">
				<div class="caption">
            		 <p>CnC Melon Juice</p>
     		   </div>
		      <img src="Images/Carousel/2.jpg" class="d-block w-100 h-100" alt="Cake2">
		    </div>
		    <div class="carousel-item">
				<div class="caption">
            		 <p>CnC Cheesy Natchos</p>
     		   </div>
		      <img src="Images/Carousel/3.jpg" class="d-block w-100 h-100" alt="Cake3">
		    </div>
		    <div class="carousel-item">
				<div class="caption">
            		 <p>CnC Cheesy </p>
     		   </div>
		      <img src="Images/Carousel/4.jpg" class="d-block w-100 h-100" alt="Cake4">
		    </div>
		    <div class="carousel-item">
				<div class="caption">
            		 <p>CnC Frappe</p>
     		   </div>
		      <img src="Images/Carousel/5.jpg" class="d-block w-100 h-100" alt="Cake5">
		    </div>
		    <div class="carousel-item">
				<div class="caption">
            		 <p>CnC All in all cupcakes</p>
     		   </div>
		      <img src="Images/Carousel/6.jpg" class="d-block w-100 h-100" alt="Cake6">
		    </div>
		    <div class="carousel-item">
				<div class="caption">
            		 <p>Juice</p>
     		   </div>
		      <img src="Images/Carousel/7.jpg" class="d-block w-100 h-100" alt="Cake7">
		    </div>
		    <div class="carousel-item">
				<div class="caption">
            		 <p>CnC Streusel</p>
     		   </div>
		      <img src="Images/Carousel/8.jpg" class="d-block w-100 h-100" alt="Cake8">
		    </div>
		    <div class="carousel-item">
				<div class="caption">
            		 <p>CnC Potato fry</p>
     		   </div>
		      <img src="Images/Carousel/9.jpg" class="d-block w-100 h-100" alt="Cake9">
		    </div>
		    <div class="carousel-item">
				<div class="caption">
            		 <p>CnC Melode Juice</p>
     		   </div>
		      <img src="Images/Carousel/10.jpg" class="d-block w-100 h-100" alt="Cake10">
		    </div>
		    <div class="carousel-item">
		      <img src="Images/Carousel/11.jpg" class="d-block w-100 h-100" alt="Cake11">
		      <div class="caption">
            		 <p>CnC Blueberry Cake</p>
     		   </div>
		    </div>
		     <div class="carousel-item">
		      <img src="Images/Carousel/12.jpg" class="d-block w-100 h-100" alt="Cake11">
		      <div class="caption">
            		 <p>CnC Streusel</p>
     		  </div>
		    </div>
		    <div class="carousel-item">
		      <img src="Images/Carousel/13.jpg" class="d-block w-100 h-100" alt="Cake11">
		      <div class="caption">
            		 <p>CnC double date bundle juice</p>
     		  </div>
		    </div>
		  </div>

		  		<a class="carousel-control-next" href="#carouselExampleInterval" role="button" data-slide="next">
		  		  <span class="next round" aria-hidden="true" onclick="AddOpacNav()">&#8250;</span>
		  		  <!-- <span class="sr-only">Next</span> -->
		  		</a>


			  <a class="carousel-control-prev" href="#carouselExampleInterval" role="button" data-slide="prev" >
			    <span class="previous round" aria-hidden="true" onclick="AddOpacNav()" >&#8249;</span>
			    <!-- <span class="sr-only">Previous</span> -->
			  </a>
			
		</div>
</section>

<style>
.caption {
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0, 0, 0, 0.4); /* Black w/opacity/see-through */
    border-radius: 10px;
    color: white;
    font-weight: bold;
    position: absolute;
    top: 70%;
    left: 50%;
    transform: translate(-50%, -50%);
    z-index: 2;
    width: 50%;
    padding: 40px;
    text-align: center;
    font-size:50px
	}
</style>