<footer class="bg-pastelPink" id="sticky-footer" class="py-4 text-white-50">
  <div class="footer-wrapper">
    <div class="container-fluid">
          <div class="footer-mid-part">
            <div class="row">
              <div class="col-lg-3 col-md-6 col-sm-12">

                <div class="footer-section-one">
       <div class="footer-heading"><h2>Contact Us</h2></div>
                  <div class="footer-contact-box">
                    <div class="footer-contact-icon"><i class="fa fa-map-marker"></i></div>
                      <div class="footer-contact-text">
                        <p>2nd Flr. Regina Bldg., Maragondon , Cavite Maragondon, Cavite</p>
                      </div>
                    <div class="clr"></div>
                  </div>

                   <div class="footer-contact-box">
                    <div class="footer-contact-icon"><i class="fa fa-phone"></i></div>
                      <div class="footer-contact-text">
                        <p>(+63) 917 865 8922</p>
                      </div>
                    <div class="clr"></div>
                   </div>

                   <div class="footer-contact-box">
                    <div class="footer-contact-icon"><i class="fa fa-wechat"></i></div>
                      <div class="footer-contact-text">
                        <p><a href="https://www.Facebook.com/messages/t/chaceandcherrie">Facebook.com/messages/t/chaceandcherrie</a></p>
                      </div>
                    <div class="clr"></div>
                   </div>

                  <!--  <div class="footer-contact-box">
                    <div class="footer-contact-icon"><i class="fa fa-envelope"></i></div>
                      <div class="footer-contact-text">
                        <p><a href="mailto:info@learningtutorialpoingt.com">cakecake@gmail.com</a></p>
                      </div>
                    <div class="clr"></div>
                   </div> -->

                   <div class="footer-contact-box">
                    <div class="footer-contact-icon"><i class="fa fa-globe"></i></div>
                      <div class="footer-contact-text">
                         <p><a href="https://www.facebook.com/chaceandcherrie">Facebook.com/chaceandcherrie</a></p>
                      </div>
                    <div class="clr"></div>
                   </div>
                </div>

              </div>
              <div class="col-lg-6 col-md-12 col-sm-24">
                <div class="footer-section-two">
                  <div class="footer-heading"><h2>Map</h2></div>
                  
                  	<div class="footer-map-container">
                  		<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3866.6245649984858!2d120.73420321438519!3d14.275103788798429!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x33bd87b56cb01701%3A0x355eea8bbe15d40b!2sChace%20and%20Cherrie%20Cakes%20and%20Cafe!5e0!3m2!1sen!2sph!4v1570091717958!5m2!1sen!2sph" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                  	</div>
              
                </div>
              </div>


              <div class="col-lg-3 col-md-6 col-sm-12">
                <div class="footer-section-three">
                  <div class="footer-heading"><h2>Get in touch</h2></div>
                   <div class="footer-form">
                     <form class="footer-form-inline" method="post" action="#">
                       <div class="footer-form-box">
                        <input type="text" class="form-control" placeholder="Name">
                       </div>
                       <div class="footer-form-box">
                         <input type="Email" class="form-control" placeholder="Email">
                       </div>
                       <div class="footer-form-box">
                         <input type="text" class="form-control" placeholder="Phone Number">
                       </div>
                       <div class="footer-form-box">
                         <button type="submit" class="btn btn-pastelPink">Submit</button>
                       </div>
                     </form>
                   </div>

                </div>
              </div>
            </div>
          </div>

       <div class="footer-bottom">
         <div class="row">
           <div class="col-lg-12 col-md-12 col-sm-12">
             <p>Copyright &copy; 2019 Cakes All Rights Reserved</p>
           </div>
         </div>
       </div>
    </div>
  </div>
</footer>
