<!-- license script -->
<script src="JS/scrollreveal.js"></script>
<!-- license script -->
<link rel="stylesheet" type="text/css" href="style.css">

<section id="samplecake"> <!--  for scrollspy-->

	<!-- Image Grid Begins -->
		<section class="section-padding" id="ImageGrid" >
			<div class="container bg-pink">
				 <div class="row">
				 	<div class="col-sm-12">
				 		<br><br>
				 		<h2 class="title text-center"> 
				 			Our own <span class="decorate">Cakes</span> Special for you
				 		</h2>
				 	</div>
				 </div>

				 <div class="container">
				 	<div id="my-grid" class="grid-padding">
				 		<div class="row">
				 			<!-- First Colum -->
				 			<div class="col-sm-4">

				 				<div class="row showcase-left"><img src="Images/Chocolate/1.jpg" class="fit-image" onclick="openModal();currentSlide(1)">
				 				</div>
				 				<div class="row showcase-right"><img src="Images/Chocolate/2.jpg" class="fit-image" onclick="openModal();currentSlide(2)">
				 				</div>
				 				<div class="row showcase-top"><img src="Images/Chocolate/3.jpg" class="fit-image" onclick="openModal();currentSlide(3)">
				 				</div>
				 				<div class="row showcase-bottom"><img src="Images/Chocolate/4.jpg" class="fit-image" onclick="openModal();currentSlide(4)">
				 				</div>

				 			</div>
				 			<!-- Second Column -->
				 			<div class="col-sm-4">

				 				<div class="row showcase-top"><img src="Images/Chocolate/5.jpg" class="fit-image" onclick="openModal();currentSlide(5)">
				 				</div>
				 				<div class="row showcase-bottom"><img src="Images/Chocolate/6.jpg" class="fit-image" onclick="openModal();currentSlide(6)">
				 				</div>
				 				<div class="row showcase-left"><img src="Images/Chocolate/7.jpg" class="fit-image" onclick="openModal();currentSlide(7)">
				 				</div>
				 				<div class="row showcase-right"><img src="Images/Chocolate/8.jpg" class="fit-image" onclick="openModal();currentSlide(8)">
				 				</div>
				 				
				 			</div>
				 			<!-- Third Column -->
				 			<div class="col-sm-4">

				 				<div class="row showcase-right"><img src="Images/Chocolate/9.jpg" class="fit-image" onclick="openModal();currentSlide(9)">
				 				</div>
				 				<div class="row showcase-bottom"><img src="Images/Chocolate/10.jpg" class="fit-image" onclick="openModal();currentSlide(10)">
				 				</div>
				 				<div class="row showcase-top"><img src="Images/Chocolate/11.jpg" class="fit-image" onclick="openModal();currentSlide(11)">
				 				</div>
				 				<div class="row showcase-left"><img src="Images/Chocolate/12.jpg" class="fit-image" onclick="openModal();currentSlide(12">
				 				</div>
				 				
				 			</div>
				 		</div>
				 	</div>
				 </div>
			</div>
		</section>
	<!-- Image Grid Ends -->

	<!-- Modal for Full Screen Image -->
		<div id="my-modal" class="modal">
			<!-- close button -->
			<span class="exit" onclick="exitModal()">&times;</span>
			<!-- Modal -->
			<div class="modal-content">
				<!-- Image Viewer -->
				<div class="my-slides">
					<div class="numbertext">1/12</div>
					<img src="Images/Chocolate/1.jpg" class="landscape-image">
				</div>
				<div class="my-slides">
					<div class="numbertext">2/12</div>
					<img src="Images/Chocolate/2.jpg" class="landscape-image">
				</div>
				<div class="my-slides">
					<div class="numbertext">3/12</div>
					<img src="Images/Chocolate/3.jpg" class="landscape-image">
				</div>
				<div class="my-slides">
					<div class="numbertext">4/12</div>
					<img src="Images/Chocolate/4.jpg" class="portrait-image">
				</div>
				<div class="my-slides">
					<div class="numbertext">5/12</div>
					<img src="Images/Chocolate/5.jpg" class="landscape-image">
				</div>
				<div class="my-slides">
					<div class="numbertext">6/12</div>
					<img src="Images/Chocolate/6.jpg" class="landscape-image">
				</div>
				<div class="my-slides">
					<div class="numbertext">7/12</div>
					<img src="Images/Chocolate/7.jpg" class="landscape-image">
				</div>
				<div class="my-slides">
					<div class="numbertext">8/12</div>
					<img src="Images/Chocolate/8.jpg" class="landscape-image">
				</div>
				<div class="my-slides">
					<div class="numbertext">9/12</div>
					<img src="Images/Chocolate/9.jpg" class="landscape-image">
				</div>
				<div class="my-slides">
					<div class="numbertext">10/12</div>
					<img src="Images/Chocolate/10.jpg" class="landscape-image">
				</div>
				<div class="my-slides">
					<div class="numbertext">11/12</div>
					<img src="Images/Chocolate/11.jpg" class="landscape-image">
				</div>
				<div class="my-slides">
					<div class="numbertext">12/12</div>
					<img src="Images/Chocolate/12.jpg" class="landscape-image">
				</div>
				<!-- Next and Prev -->
				<a class="prevbutton" onclick="changeSlides(-1)">&#10094;</a>
				<a class="nextbutton" onclick="changeSlides(1)">&#10095;</a>
			
			</div>
		</div>
	<!-- Modal for Full Screen Image -->
</section><!--  for scrollspy-->


<script type="text/javascript">

	// $('html, body').smoothScroll(1000);

	// Opening Modal script
	function openModal(){
		// Open on click
		document.getElementById('my-modal').style.display = "inline-flex";
		document.body.style.overflow = "hidden";
		document.getElementById('my-navbar').style.display = "none";
	}
	// Close modal script
	function exitModal(){
		document.getElementById('my-modal').style.display = "none";
		document.body.style.overflow = "auto";
		document.getElementById('my-navbar').style.display = "flex";
	}

	// variable for slide
	var slideIndexJS = 1;

	// Calling function
	showSlides( slideIndexJS );

	// Next and prevt script
	function changeSlides(n){
		showSlides(slideIndexJS += n);
	}
	// Display Image that was clicked
	function currentSlide(n){
		showSlides(slideIndexJS = n);
	}

	function showSlides(n){
		var i;

		// get all class at my-slides
		var slidesJS = document.getElementsByClassName("my-slides");

		// OverFlow
		if (n > slidesJS.length) { slideIndexJS = 1; }
		// UnderFlow
		if (n < 1) {slideIndexJS = slidesJS.length;  }

		// hide image
		for (i = 0; i < slidesJS.length; i++){
			slidesJS[i].style.display  = "none";
		}

		// showing slide which click
		slidesJS[slideIndexJS - 1].style.display = "block";
	}
</script>

