<div class="slideshow-container">

  <div class="mySlides">
    <h1>Monlade</h1>
    <h3>Cold drink suit for your taste</h3>
    <img src="Images/NewAndUpdate/1.jpg" height="500" width="500">
  </div>

  <div class="mySlides">
    <h1>Orakes</h1>
    <h3>Dominated with oreo cookies</h3>
    <img src="Images/NewAndUpdate/2.jpg" height="500" width="500">
  </div>

  <div class="mySlides">
    <h1>BundleCups</h1>
    <h3>New meal suited for barkada</h3>
    <img src="Images/NewAndUpdate/3.jpg" height="500" width="500">
  </div>

  <!-- <span class="prev" onclick="plusSlides(-1)">&#8249;</span>
  <span class="next" onclick="plusSlides(1)">&#8250;</span> -->

</div>

<div class="dot-container">
  <span class="dot" onclick="currentSlide(1)"></span> 
  <span class="dot" onclick="currentSlide(2)"></span> 
  <span class="dot" onclick="currentSlide(3)"></span> 
</div>

<script>
var slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("dot");
  if (n > slides.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";  
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";  
  dots[slideIndex-1].className += " active";
}
</script>
