<!-- Social Media Buttons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!-- Social Media Buttons -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="CSS/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="style.css">

<nav class="navbar navbar-expand-sm navbar-light sticky-top bg-pastelPink " id="my-navbar">
	 <a class="navbar-brand nav-item" href="Index.php"><img class="OwnLogo"src="images/FinalLogo.png" onclick=" ShowNavLink()" alt="Logo" style="width: 100px;"></a>
	 <button class="navbar-toggler " type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
	   <span class="navbar-toggler-icon"></span>
	 </button>

	 <div class="collapse navbar-collapse" id="navbarSupportedContent">
	   <ul class="navbar-nav mr-auto navdrop"  id="navlinks">

			  <?php if(!isset($_SESSION['username'])){ ?>
				   <li class="nav-item"><a class="nav-link nav-menu" href="Index.php">Home </a></li>
		  <ul>

				   <li class="nav-item"><a class="nav-link " href="#"> Products </a>
					   <ul>
						  <li class="nav-item"><a class="nav-link " href="Cake.php"> Chocolate </a>	
						  <li class="nav-item"><a class="nav-link " href="Coffee.php"> Coffee </a>	
						  <li class="nav-item"><a class="nav-link " href="CupCakes.php"> Pastries </a>	
					   </ul>
				   </li>
				   


			   <?php } ?> 
		  </ul>

	   </ul>

	 </div>
</nav>

<script type="text/javascript" src="JS/bootstrap.bundle.min.js"></script>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>

