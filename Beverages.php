<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Coffee</title>
  <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
   <!-- Social Media Buttons -->
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
 <!-- Social Media Buttons -->
  <link rel="stylesheet" href="./SwiperCSS/swiper.min.css">
  <link rel="stylesheet" type="text/css" href="CSS/bootstrap.min.css">
  <link rel="stylesheet" href="style.css">

  <!-- Dont transfer to style.css because of same define name with differ function -->
  <style>
    #introduction{
      margin-top:5%;
    }
    #CoffeeQoutes{
    background-color:  #ffd1dc !important;
    padding: 50px 0 30px 0;
    text-align:center;
    margin-top:40px;
    }

    #CoffeeQoutes p{
      font-size:32px;
      color:#000;
    }

    #CoffeeQoutes p.writer{
     font-size:20px;
     color:#666;
    }

    .caption {
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0, 0, 0, 0.4); /* Black w/opacity/see-through */
    color: white;
    font-weight: bold;
    position: absolute;
    top: 85%;
    left: 50%;
    transform: translate(-50%, -50%);
    z-index: 2;
    width: 50%;
    padding: 40px;
    text-align: center;
    font-size:50px
    }

    body {
      font-size: 14px;
    }

    .BeveragesCaption {
    background-color: rgba( 0, 0, 0, 0.2); /* Black w/opacity/see-through */
    border-radius: 10px;
    color: white;
    font-weight: bold;
    position: absolute;
    top: 80%;
    left: 50%;
    transform: translate(-50%, -50%);
    z-index: 2;
    width: 50%;
    padding: 40px;
    text-align: center;
    font-size:50px
    }
    
    .TopSpace{
      margin-top:1%;
    }

    .fit-image{
			height: 100%;
			width: 100%;
			top: 0;
			}
    .swiper-container {
      
      width: 60%;
      height: 15% !important ;
      margin-left: auto;
      margin-right: auto;
    }
    .swiper-slide {
      background-size: cover;
      background-position: center;
    }
    .gallery-top {
      height: 40%;
      width: 60%;
    }
    .gallery-thumbs {
      height: 20% !important;
      box-sizing: border-box;
      padding: 10px 0;
    }
    .gallery-thumbs .swiper-slide {
      width: 25%;
      height: 100%;
      opacity: 0.4;
    }
    .gallery-thumbs .swiper-slide-thumb-active {
      opacity: 1;
    }
  </style>

</head>
<body>

<nav class="navbar navbar-expand-sm navbar-light fixed-top bg-pastelPink " id="my-navbar">
	 <a class="navbar-brand nav-item" href="Index.php"><img class="OwnLogo"src="images/FinalLogo.png" onclick=" ShowNavLink()" alt="Logo" style="width: 100px;"></a>
	 <button class="navbar-toggler " type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
	   <span class="navbar-toggler-icon"></span>
	 </button>

	 <div class="collapse navbar-collapse" id="navbarSupportedContent">
	   <ul class="navbar-nav mr-auto navdrop"  id="navlinks">

			  <?php if(!isset($_SESSION['username'])){ ?>
				   <li class="nav-item"><a class="nav-link nav-menu" href="Index.php">Home </a></li>
		  <ul>

				   <li class="nav-item"><a class="nav-link " href="#"> Products </a>
					   <ul>
						  <li class="nav-item"><a class="nav-link " href="Dessert.php"> Dessert </a>	
						  <li class="nav-item"><a class="nav-link " href="Beverages.php"> Beverages </a>	
						  <li class="nav-item"><a class="nav-link " href="CupCakes.php"> Pastries </a>	
					   </ul>
				   </li>
				   


			   <?php } ?> 
		  </ul>

	   </ul>

	 </div>
</nav>




<section id="introduction">

  <div class="Coffeebgimg-1">
          
  </div>

  <div class="caption">
     <p>Chace and Cherrie Beverages</p>
  </div>

     
</section>
    

 <div class="showcase-right" id="CoffeeQoutes">
        <div class="container">
          <p>"Science may never come up with a better office communication system than the coffee break."</p>
          <p class="writer">- Earl Wilson</p>
        </div>
      </div>

  <!-- Swiper -->
  <div class="swiper-container gallery-top TopSpace showcase-left">
    <div class="swiper-wrapper">
      <div class="swiper-slide">
        <div class="BeveragesCaption">
             <p>Beverage 1</p>
        </div>
         <img class="fit-image" src="./Images/Coffee/1.jpg">
      </div>
      <div class="swiper-slide">
        <div class="BeveragesCaption">
             <p>Beverage 2</p>
        </div>
         <img class="fit-image" src="./Images/Coffee/2.jpg">
      </div>
      <div class="swiper-slide">
         <div class="BeveragesCaption">
             <p>Beverage 3</p>
        </div>
         <img class="fit-image" src="./Images/Coffee/3.jpg">
      </div>
      <div class="swiper-slide">
         <div class="BeveragesCaption">
             <p>Beverage 4</p>
        </div>
         <img class="fit-image" src="./Images/Coffee/4.jpg">
      </div>
      <div class="swiper-slide">
         <div class="BeveragesCaption">
             <p>Beverage 5</p>
        </div>
         <img class="fit-image" src="./Images/Coffee/5.jpg">
      </div>
      <div class="swiper-slide">
        <div class="BeveragesCaption">
             <p>Beverage 6</p>
        </div>
         <img class="fit-image" src="./Images/Coffee/6.jpg">
      </div>
      <div class="swiper-slide">
        <div class="BeveragesCaption">
             <p>Beverage 7</p>
        </div>
         <img class="fit-image" src="./Images/Coffee/7.jpg">
      </div>
      <div class="swiper-slide">
        <div class="BeveragesCaption">
             <p>Beverage 8</p>
        </div>
         <img class="fit-image" src="./Images/Coffee/8.jpg">
      </div>
      <div class="swiper-slide">
        <div class="BeveragesCaption">
             <p>Beverage 9</p>
        </div>
         <img class="fit-image" src="./Images/Coffee/9.jpg">
      </div>
      <div class="swiper-slide">
        <div class="BeveragesCaption">
             <p>Beverage 10</p>
        </div>
         <img class="fit-image" src="./Images/Coffee/10.jpg">
      </div>
      <div class="swiper-slide">
        <div class="BeveragesCaption">
             <p>Beverage 11</p>
        </div>
         <img class="fit-image" src="./Images/Coffee/11.jpg">
      </div>
      <div class="swiper-slide">
        <div class="BeveragesCaption">
             <p>Beverage 12</p>
        </div>
         <img class="fit-image" src="./Images/Coffee/12.jpg">
      </div>
      <div class="swiper-slide">
        <div class="BeveragesCaption">
             <p>Beverage 13</p>
        </div>
         <img class="fit-image" src="./Images/Coffee/13.jpg">
      </div>
      <div class="swiper-slide">
        <div class="BeveragesCaption">
             <p>Beverage 14</p>
        </div>
         <img class="fit-image" src="./Images/Coffee/14.jpg">
      </div>
      <div class="swiper-slide">
        <div class="BeveragesCaption">
             <p>Beverage 15</p>
        </div>
         <img class="fit-image" src="./Images/Coffee/15.jpg">
      </div>
       <div class="swiper-slide">
        <div class="BeveragesCaption">
             <p>Beverage 16</p>
        </div>
         <img class="fit-image" src="./Images/Coffee/16.jpg">
      </div>
    </div>
    <!-- Add Arrows -->
    <div class="swiper-button-next swiper-button-white"></div>
    <div class="swiper-button-prev swiper-button-white"></div>
  </div>

  <div class="swiper-container gallery-thumbs showcase-right">
    <div class="swiper-wrapper">
      <div class="swiper-slide"> <img class="fit-image" src="./Images/Coffee/1.jpg"></div>
      <div class="swiper-slide"> <img class="fit-image" src="./Images/Coffee/2.jpg"></div>
      <div class="swiper-slide"> <img class="fit-image" src="./Images/Coffee/3.jpg"></div>
      <div class="swiper-slide"> <img class="fit-image" src="./Images/Coffee/4.jpg"></div>
      <div class="swiper-slide"> <img class="fit-image" src="./Images/Coffee/5.jpg"></div>
      <div class="swiper-slide"> <img class="fit-image" src="./Images/Coffee/6.jpg"></div>
      <div class="swiper-slide"> <img class="fit-image" src="./Images/Coffee/7.jpg"></div>
      <div class="swiper-slide"> <img class="fit-image" src="./Images/Coffee/8.jpg"></div>
      <div class="swiper-slide"> <img class="fit-image" src="./Images/Coffee/9.jpg"></div>
      <div class="swiper-slide"> <img class="fit-image" src="./Images/Coffee/10.jpg"></div>
      <div class="swiper-slide"> <img class="fit-image" src="./Images/Coffee/11.jpg"></div>
      <div class="swiper-slide"> <img class="fit-image" src="./Images/Coffee/12.jpg"></div>
      <div class="swiper-slide"> <img class="fit-image" src="./Images/Coffee/13.jpg"></div>
      <div class="swiper-slide"> <img class="fit-image" src="./Images/Coffee/14.jpg"></div>
      <div class="swiper-slide"> <img class="fit-image" src="./Images/Coffee/15.jpg"></div>
      <div class="swiper-slide"> <img class="fit-image" src="./Images/Coffee/16.jpg"></div>
    </div>
  </div>

  <header><?php include ( "./include/Footer.php" ); ?></header>

  

<!-- license script -->
<script src="JS/scrollreveal.js"></script>
<!-- license script -->


<!-- Own Script -->
<script type="text/javascript" src="JS/ScrollAnimation.js"></script>
<script type="text/javascript" src="JS/mainscript.js"></script>
<script type="text/javascript" src="JS/backtotop.js"></script>
<!-- Own Script -->
<script type="text/javascript" src="JS/jquery.smoothscroll.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" src="JS/bootstrap.bundle.js"></script>
<script type="text/javascript" src="JS/bootstrap.bundle.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<!-- Cake.php -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" src="JS/jquery.smoothscroll.js"></script>
<!-- Cake.php -->

  <!-- Swiper JS -->
  <script src="./SwiperJS/swiper.min.js"></script>

  <!-- Initialize Swiper -->
  <script>
    var galleryThumbs = new Swiper('.gallery-thumbs', {
      spaceBetween: 10,
      slidesPerView: 4,
      freeMode: true,
      watchSlidesVisibility: true,
      watchSlidesProgress: true,
    });
    var galleryTop = new Swiper('.gallery-top', {
      spaceBetween: 10,
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
      thumbs: {
        swiper: galleryThumbs
      }
    });
  </script>
</body>
</html>